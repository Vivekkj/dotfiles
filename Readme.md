# Dot files of Vivek K J
### This repo contains dotfiles which is being used by me

## Screenshot

![1](./screenshots/1.png)
![2](./screenshots/2.png)

## Specifications
Desktop Environment: KDE

Window Manager: i3-gaps

Compositor: picom

Bar: Polybar, KDE Panel (bottom)

Application Launcher: Rofi

Shell: OhMyZSH + Powerlevel10k + [colorscript](https://gitlab.com/dwt1/shell-color-scripts)

## Location for Files

`/usr/share/xsessions/plasma-i3.desktop`

`.config/{i3,polybar,rofi, plasma-org.kde.desktop}`

`~/{picom.conf, .p10k.zsh, .zshrc, gpg.conf}`

`/etc/fstab ## For automounting my local drive`

## Dependencies

```
  * i3-gaps
  * kde
  * ohmyzsh
  * rofi
  * polybar
  * feh (For wallpaper)
  * picom
  * redshift (For night mode / reading mode)
```
